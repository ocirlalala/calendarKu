from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

class App1Test(TestCase):
	def test_app_1_url_is_exist(self):
		response = Client().get('/app-1/')
		self.assertEqual(response.status_code,200)

	def test_app_1_using_index_func(self):
		found = resolve('/app-1/')
		self.assertEqual(found.func, index)